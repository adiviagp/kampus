<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\UserResource;

class CourseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
        'id' => $this->id,
        'user_id' => $this->user_id,
        'name' => $this->name,
        'status' => $this->status,
        'dosen' => new UserResource($this->dosen),
        'credit' => $this->credit,
        'created_at' => $this->created_at,
      ];
    }
}
