<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PeriodeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
        'id' => $this->id,
        'year' => $this->year,
        'semester' => $this->semester,
        'register_start' => $this->register_start,
        'register_end' => $this->register_end,
        'created_at' => $this->created_at,

      ];
    }
}
