<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StudyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id' => $this->id,
          'user_id' => $this->user_id,
          'periode_id' => $this->periode_id,
          'course_id' => $this->course_id,
          'grade' => $this->grade,
          'mahasiswa' => new UserResource($this->mahasiswa),
          'course' => new CourseResource($this->course),
          'periode' => new PeriodeResource($this->periode),
          'created_at' => $this->created_at,
        ];
    }
}
