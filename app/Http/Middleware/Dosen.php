<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class Dosen
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if (! Auth::user()->role == 'dosen') {
        return response(["message" => "Unauthorized"], 500);
      }
      return $next($request);
   }
}
