<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ... $roles)
    {

        $user = Auth::User();

        foreach($roles as $role) {
          if ($user->hasRole($role)) {
            return $next($request);
          }
        }

        return response(["message" => "Unauthorized"], 500);

    }
}
