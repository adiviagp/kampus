<?php

namespace App\Http\Controllers;

use App\Model\Periode;

use Illuminate\Http\Request;
use App\Http\Resources\PeriodeResource;

/**
 * @group Periode
 *
 * APIs for managing periode
 */

class PeriodeController extends Controller
{

    public function __construct()
    {
        $this->middleware('JWT');
        $this->authorizeResource(Periode::class, 'periode');
    }

    /**
     * Fetch All Periode
     *
     * Menampilkan semua Periode
     *
      * @apiResourceCollection App\Http\Resources\PeriodeResource
      * @apiResourceModel App\Model\Periode
    */

    public function index()
    {
        return PeriodeResource::collection(Periode::latest()->get());
    }

    /**
     * Store Periode
     *
     * Menambah Periode
     * @faker_seed
     * @bodyParam year int required The year. Example: 2020
     * @bodyParam semester int required The semester. Example: 6
     * @bodyParam register_start date required The register_start. Example: 2020-01-01
     * @bodyParam register_end date required The register_end. Example: 2020-02-02
     */

    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'year' => 'required',
            'semester' => 'required',
            'register_start' => 'required',
            'register_end' => 'required',
        ]);

        Periode::create($request->all());
        return response()->json(['message' => 'Data berhasil ditambah'], 200);

    }

    /**
     * Fetch Periode
     *
     * Menampilan Periode yang dipilih
     *
      * @apiResourceCollection App\Http\Resources\PeriodeResource
      * @apiResourceModel App\Model\Periode
    */

    public function show(Periode $periode)
    {
        return new PeriodeResource($periode);
    }

    /**
     * Update Periode
     *
     * Mengupdate Periode yang dipilih
     *
     * @queryParam year int: The year. Example: 2020
     * @queryParam semester int: The semester. Example: 6
     * @queryParam register_start date: The register_start. Example: 2020-01-01
     * @queryParam register_end date: The register_end. Example: 2020-02-02

     */

    public function update(Request $request, Periode $periode)
    {
        $periode->update($request->all());
        return response()->json(['message' => 'Data berhasil diupdate'], 200);
    }

    /**
     * Delete Periode
     *
     * Menghapus cource yang dipilih

     */

    public function destroy(Periode $periode)
    {
        $periode->delete();
        return response()->json(['message' => 'Data berhasil dihapus'], 200);
    }

    /**
     * Periode AKtif
     *
     * Mengetahui semester yang aktif


     */

    public function semesterBerapa(){
        $periode = Periode::latest()->first();
        return new PeriodeResource($periode);
    }
}
