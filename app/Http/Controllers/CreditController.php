<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Credit;
use App\Http\Resources\CreditResource;

/**
 * @group Credit
 *
 * APIs for managing credit
*/

class CreditController extends Controller
{
    public function __construct()
    {
        $this->middleware('JWT');
    }

    /**
     * Fetch All Credit
     *
     * Menampilkan semua Credit
     *
      * @apiResourceCollection App\Http\Resources\CreditResource
      * @apiResourceModel App\Model\Credit
    */

    public function index()
    {
      $this->authorize('viewAny', Credit::class);
      return CreditResource::collection(Credit::latest()->get());
    }

    /**
     * Store Credit
     *
     * Menambah Credit
     * @faker_seed
     * @bodyParam user_id int required The user_id. Example: 2
     * @bodyParam credit int required The credit. Example: 6

     */

    public function store(Request $request)
    {
        $this->authorize('create', Credit::class);

        $validatedData = $request->validate([
            'user_id' => 'required',
            'credit' => 'required',
        ]);

        Credit::create($request->all());
        return response()->json(['message' => 'sukses'], 200);
    }

    /**
     * Fetch Credit
     *
     * Menampilan Credit yang dipilih
     *
      * @apiResourceCollection App\Http\Resources\CreditResource
      * @apiResourceModel App\Model\Credit
    */

    public function show(Credit $credit)
    {
      $this->authorize('view', $credit);
      return new CreditResource($credit);
    }

    /**
     * Update Credit
     *
     * Mengupdate Credit yang dipilih
     *
     * @faker_seed
     * @queryParam user_id int: The user_id. Example: 1
     * @queryParam credit int: The credit. Example: 6
     */

    public function update(Request $request, Credit $credit)
    {
        $this->authorize('update', $credit);
        $credit->update($request->all());
        return response()->json(['message' => 'Data berhasil di update'], 200);
    }

    /**
     * Delete Credit
     *
     * Menghapus cource yang dipilih

     */

    public function destroy(Credit $credit)
    {
        $this->authorize('delete', $credit);
        $credit->delete();
        return response()->json(['message' => 'Data berhasil di hapus'],200);
    }
}
