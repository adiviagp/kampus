<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Resources\UserResource;

/**
 * @group Auth
 *
 * APIs for managing auth
 */
class AuthController extends Controller
{
  public function __construct()
  {
      $this->middleware('JWT', ['except' => ['login','signup']]);
  }

  /**
   * Login
   *
   * login untuk Authentication & authorization
   *
   * @bodyParam username string required username. Example: adiviagp
   * @bodyParam password string required password. Example: 12345678

   */

  public function login()
  {
      $credentials = request(['username', 'password']);

      if (! $token = auth()->attempt($credentials)) {
          return response()->json(['error' => 'Unauthorized'], 401);
      }

      return $this->respondWithToken($token);
  }

  /**
   * Signup
   *
   * Mendaftarkan user ke sistem
   *
   * @bodyParam name string required name. Example: adiviagp
   * @bodyParam username string required username. Example: adiviagp
   * @bodyParam password string required password. Example: 12345678
   * @bodyParam role string required role. Example: mahasiswa

   */

  public function signup(Request $request)
  {
    User::create($request->all());
    return $this->login($request);
  }

  /**
   * User who logging in
   *
   * Mengetahui data dari user yang telah login
   *
   * @apiResourceCollection App\Http\Resources\UserResource
   * @apiResourceModel App\User
   *
  */

  public function me()
  {
      return new UserResource(auth()->user());
  }

  /**
   * Log the user out (Invalidate the token).
   *
   * Keluar dari sistem
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function logout()
  {
      auth()->logout();

      return response()->json(['message' => 'Successfully logged out']);
  }

  /**
   * Refresh a token.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function refresh()
  {
      return $this->respondWithToken(auth()->refresh());
  }

  /**
   * Get the token array structure.
   *
   * @param  string $token
   *
   * @return \Illuminate\Http\JsonResponse
   */
  protected function respondWithToken($token)
  {
      return response()->json([
          'access_token' => $token,
          'token_type' => 'bearer',
          'expires_in' => auth()->factory()->getTTL() * 60
      ]);
  }

}
