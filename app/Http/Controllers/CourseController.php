<?php

namespace App\Http\Controllers;

use App\Model\Course;
use App\Model\Study;

use Illuminate\Http\Request;
use App\Http\Resources\CourseResource;

/**
 * @group Course
 *
 * APIs for managing course
 */

class CourseController extends Controller
{


    public function __construct()
    {
        $this->middleware('JWT');
    }

    /**
     * Fetch All Course
     *
     * Menampilkan semua course
     *
      * @apiResourceCollection App\Http\Resources\CourseResource
      * @apiResourceModel App\Model\Course
    */

    public function index()
    {
        $this->authorize('viewAny', Course::class);
        return CourseResource::collection(Course::latest()->get());
    }

    /**
     * Store Course
     *
     * Menambah Course
     * @faker_seed
     * @bodyParam user_id int required The user_id. Example: 1
     * @bodyParam name string required The name. Example: PABW
     * @bodyParam credit int required The credit. Example: 6

     */

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'user_id' => 'required',
            'name' => 'required',
            'credit' => 'required',
        ]);

        $this->authorize('create', Course::class);
        Course::create($request->all());
        return response()->json(['message' => 'Data berhasil di tambah'], 200);
    }

    /**
     * Fetch Course
     *
     * Menampilan course yang dipilih
     *
      * @apiResourceCollection App\Http\Resources\CourseResource
      * @apiResourceModel App\Model\Course
    */

    public function show(Course $course)
    {
        $this->authorize('view', $course);
        return new CourseResource($course);
    }

    /**
     * Update Course
     *
     * Mengupdate course yang dipilih
     *
     * @faker_seed
     * @queryParam user_id int: The user_id. Example: 1
     * @queryParam name string: The name. Example: PABW
     * @queryParam credit int: The credit. Example: 6
     */


    public function update(Request $request, Course $course)
    {
        $this->authorize('update', $course);
        $course->update($request->all());
        return response()->json(['message' => 'Data berhasil di update'],200);
    }

    /**
     * Delete Course
     *
     * Menghapus cource yang dipilih

     */

    public function destroy(Course $course)
    {
        $this->authorize('delete', $course);
        $course->delete();
        return response()->json(['message' => 'Data berhasil di hapus'],200);
    }

    /**
     * User's Courses
     *
     * Mendapatkan course yang telah diambil oleh user (mahasiswa).
     *

     */

    public function userCourses($id){
        $study = Study::where('user_id', $id)->get();
        $userCourses = [];

        foreach($study as $study){
          $userCourses[] = [$study->course, ["study_id" => $study->id, "grade" => $study->grade]];
        }

        return $userCourses;
    }
}
