<?php

namespace App\Http\Controllers;

use App\Model\Study;
use App\Model\Periode;

use Illuminate\Http\Request;
use App\Http\Resources\StudyResource;

/**
 * @group Study
 *
 * APIs for managing study
 */

class StudyController extends Controller
{
    public function __construct()
    {
        $this->middleware('JWT');
        $this->authorizeResource(Study::class, 'study');
    }

    /**
     * Fetch All Study
     *
     * Menampilkan semua Study
     *
      * @apiResourceCollection App\Http\Resources\StudyResource
      * @apiResourceModel App\Model\Study
    */

    public function index()
    {
      return StudyResource::collection(Study::latest()->get());
    }

    /**
     * Store Study
     *
     * Menambah Study
     * @faker_seed
     * @bodyParam user_id int required The user_id. Example: 1
     * @bodyParam periode_id int required The periode_id. Example: 3
     * @bodyParam course_id int required The study_id. Example: 6

     */

    public function store(Request $request)
    {
      $expected_at = Periode::first();
      $validatedData = $request->validate([
          'user_id' => 'required',
          'periode_id' => 'required',
          'course_id' => 'required',
          'created_at' => 'date|before_or_equal:'.$expected_at->register_end,
      ]);

      Study::create($request->all());
      return response()->json(['message' => 'Data berhasil ditambah'], 200);

    }

    /**
     * Fetch Study
     *
     * Menampilan Study yang dipilih
     *
      * @apiResourceCollection App\Http\Resources\StudyResource
      * @apiResourceModel App\Model\Study
    */

    public function show(Study $study)
    {
        return new StudyResource($study);
    }

    /**
     * Update Study
     *
     * Mengupdate Study yang dipilih
     * @queryParam user_id int: The user_id. Example: 1
     * @queryParam periode_id int: The periode_id. Example: 3
     * @queryParam course_id int: The course_id. Example: 6
     * @queryParam grade int: The semester. Example: 6
     */


    public function update(Request $request, Study $study)
    {
        $study->update($request->all());
        return response()->json(['message' => 'Data berhasil diupdate'], 200);

    }


        /**
         * Delete Study
         *
         * Menghapus cource yang dipilih

         */

    public function destroy(Study $study)
    {
        $study->delete();
        return response()->json(['message' => 'Data berhasil dihapus'], 200);
    }

}
