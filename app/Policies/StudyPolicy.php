<?php

namespace App\Policies;

use App\Model\Study;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class StudyPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any studies.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
      return in_array($user->role, ['mahasiswa','admin']);
    }

    /**
     * Determine whether the user can view the study.
     *
     * @param  \App\User  $user
     * @param  \App\Study  $study
     * @return mixed
     */
    public function view(User $user, Study $study)
    {
      return in_array($user->role, ['mahasiswa','admin','dosen']);
    }

    /**
     * Determine whether the user can create studies.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
      return in_array($user->role, ['mahasiswa']);
    }

    /**
     * Determine whether the user can update the study.
     *
     * @param  \App\User  $user
     * @param  \App\Study  $study
     * @return mixed
     */
    public function update(User $user, Study $study)
    {
      return in_array($user->role, ['mahasiswa','dosen']);
    }

    /**
     * Determine whether the user can delete the study.
     *
     * @param  \App\User  $user
     * @param  \App\Study  $study
     * @return mixed
     */
    public function delete(User $user, Study $study)
    {
      return in_array($user->role, ['mahasiswa']);
    }

    /**
     * Determine whether the user can restore the study.
     *
     * @param  \App\User  $user
     * @param  \App\Study  $study
     * @return mixed
     */
    public function restore(User $user, Study $study)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the study.
     *
     * @param  \App\User  $user
     * @param  \App\Study  $study
     * @return mixed
     */
    public function forceDelete(User $user, Study $study)
    {
        //
    }
}
