<?php

namespace App\Policies;

use App\Model\Credit;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CreditPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any credits.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
      return in_array($user->role, ['admin']);
    }

    /**
     * Determine whether the user can view the credit.
     *
     * @param  \App\User  $user
     * @param  \App\Credit  $credit
     * @return mixed
     */
    public function view(User $user, Credit $credit)
    {
      if($user->role == "mahasiswa"){
        return $user->id === $credit->user_id;
      } else {
        return in_array($user->role, ['admin']);
      }
    }

    /**
     * Determine whether the user can create credits.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
      return in_array($user->role, ['admin']);
    }

    /**
     * Determine whether the user can update the credit.
     *
     * @param  \App\User  $user
     * @param  \App\Credit  $credit
     * @return mixed
     */
    public function update(User $user, Credit $credit)
    {
      return in_array($user->role, ['admin']);
    }

    /**
     * Determine whether the user can delete the credit.
     *
     * @param  \App\User  $user
     * @param  \App\Credit  $credit
     * @return mixed
     */
    public function delete(User $user, Credit $credit)
    {
      return in_array($user->role, ['admin']);
    }

    /**
     * Determine whether the user can restore the credit.
     *
     * @param  \App\User  $user
     * @param  \App\Credit  $credit
     * @return mixed
     */
    public function restore(User $user, Credit $credit)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the credit.
     *
     * @param  \App\User  $user
     * @param  \App\Credit  $credit
     * @return mixed
     */
    public function forceDelete(User $user, Credit $credit)
    {
        //
    }
}
