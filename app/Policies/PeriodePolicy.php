<?php

namespace App\Policies;

use App\Model\Periode;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PeriodePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any periodes.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
      return in_array($user->role, ['admin']);
    }

    /**
     * Determine whether the user can view the periode.
     *
     * @param  \App\User  $user
     * @param  \App\Periode  $periode
     * @return mixed
     */
    public function view(User $user, Periode $periode)
    {
      return in_array($user->role, ['admin']);
    }

    /**
     * Determine whether the user can create periodes.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
      return in_array($user->role, ['admin']);
    }

    /**
     * Determine whether the user can update the periode.
     *
     * @param  \App\User  $user
     * @param  \App\Periode  $periode
     * @return mixed
     */
    public function update(User $user, Periode $periode)
    {
      return in_array($user->role, ['admin']);
    }

    /**
     * Determine whether the user can delete the periode.
     *
     * @param  \App\User  $user
     * @param  \App\Periode  $periode
     * @return mixed
     */
    public function delete(User $user, Periode $periode)
    {
      return in_array($user->role, ['admin']);
    }

    /**
     * Determine whether the user can restore the periode.
     *
     * @param  \App\User  $user
     * @param  \App\Periode  $periode
     * @return mixed
     */
    public function restore(User $user, Periode $periode)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the periode.
     *
     * @param  \App\User  $user
     * @param  \App\Periode  $periode
     * @return mixed
     */
    public function forceDelete(User $user, Periode $periode)
    {
        //
    }
}
