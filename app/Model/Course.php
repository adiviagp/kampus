<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Model\Study;

class Course extends Model
{
  protected $guarded = [];

  public function dosen(){
    return $this->belongsTo(User::class, 'user_id');
  }

  public function study()
  {
    return $this->hasMany(Study::class);
  }

}
