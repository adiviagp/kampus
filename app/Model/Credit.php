<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Credit extends Model
{
    protected $guarded = [];

    public function mahasiswa(){
      return $this->belongsTo(User::class, 'user_id');
    }
}
