<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Study;

class Periode extends Model
{
  protected $guarded = [];

  public function study()
  {
    return $this->hasMany(Study::class);
  }

}
