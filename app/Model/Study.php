<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Model\Course;
use App\Model\Periode;

class Study extends Model
{
  protected $guarded = [];

  public function periode(){
    return $this->belongsTo(Periode::class, 'periode_id');
  }

  public function course(){
    return $this->belongsTo(Course::class, 'course_id');
  }

  public function mahasiswa(){
    return $this->belongsTo(User::class, 'user_id');
  }

}
