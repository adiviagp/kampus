<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\User;
use App\Http\Resources\UserResource;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::apiResource('/courses','CourseController');
Route::apiResource('/credits','CreditController');
Route::apiResource('/periodes','PeriodeController');
Route::apiResource('/studies','StudyController');


Route::get('/users', function(){
  return UserResource::collection(User::latest()->get());
})->middleware('JWT');

/**
 * @endgroup Periode
 *
 * APIs for managing periode
 */

Route::get('/user-courses/{user}', 'CourseController@userCourses');
Route::get('/semester-aktif','PeriodeController@semesterBerapa');

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::get('me', 'AuthController@me');
    Route::post('signup', 'AuthController@signup');

});
