---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#Auth


APIs for managing auth
<!-- START_a925a8d22b3615f12fca79456d286859 -->
## Login

login untuk Authentication & authorization

> Example request:

```bash
curl -X POST \
    "http://localhost/api/auth/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"username":"adiviagp","password":"12345678"}'

```

```javascript
const url = new URL(
    "http://localhost/api/auth/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "username": "adiviagp",
    "password": "12345678"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/auth/login`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `username` | string |  required  | username.
        `password` | string |  required  | password.
    
<!-- END_a925a8d22b3615f12fca79456d286859 -->

<!-- START_19ff1b6f8ce19d3c444e9b518e8f7160 -->
## Log the user out (Invalidate the token).

Keluar dari sistem

> Example request:

```bash
curl -X POST \
    "http://localhost/api/auth/logout" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/auth/logout"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/auth/logout`


<!-- END_19ff1b6f8ce19d3c444e9b518e8f7160 -->

<!-- START_994af8f47e3039ba6d6d67c09dd9e415 -->
## Refresh a token.

> Example request:

```bash
curl -X POST \
    "http://localhost/api/auth/refresh" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/auth/refresh"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/auth/refresh`


<!-- END_994af8f47e3039ba6d6d67c09dd9e415 -->

<!-- START_7c4c8c21aa8bf7ffa0ae617fb274806d -->
## User who logging in

Mengetahui data dari user yang telah login

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/auth/me" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/auth/me"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": [
        {
            "id": null,
            "name": "Dr. Janelle Stamm",
            "username": "roob.ashlynn",
            "role": "admin",
            "credit": null,
            "created_at": null
        },
        {
            "id": null,
            "name": "Mr. Coty Weimann",
            "username": "corine61",
            "role": "admin",
            "credit": null,
            "created_at": null
        }
    ]
}
```

### HTTP Request
`GET api/auth/me`


<!-- END_7c4c8c21aa8bf7ffa0ae617fb274806d -->

<!-- START_9357c0a600c785fe4f708897facae8b8 -->
## Signup

Mendaftarkan user ke sistem

> Example request:

```bash
curl -X POST \
    "http://localhost/api/auth/signup" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"adiviagp","username":"adiviagp","password":"12345678","role":"mahasiswa"}'

```

```javascript
const url = new URL(
    "http://localhost/api/auth/signup"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "adiviagp",
    "username": "adiviagp",
    "password": "12345678",
    "role": "mahasiswa"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/auth/signup`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `name` | string |  required  | name.
        `username` | string |  required  | username.
        `password` | string |  required  | password.
        `role` | string |  required  | role.
    
<!-- END_9357c0a600c785fe4f708897facae8b8 -->

#Course


APIs for managing course
<!-- START_0ec32a5c7dac7b493d908412c6b29324 -->
## Fetch All Course

Menampilkan semua course

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/courses" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/courses"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": [
        {
            "id": null,
            "user_id": 22,
            "name": "Nihil cum at ab accusamus excepturi.",
            "status": null,
            "dosen": {
                "id": 22,
                "name": "dosen",
                "username": "dosen",
                "role": "dosen",
                "credit": null,
                "created_at": null
            },
            "credit": 2,
            "created_at": null
        },
        {
            "id": null,
            "user_id": 7,
            "name": "A sit distinctio aliquam odit eos impedit velit.",
            "status": null,
            "dosen": {
                "id": 7,
                "name": "Dr. Cruz Wehner",
                "username": "harris.pauline",
                "role": "dosen",
                "credit": null,
                "created_at": "2020-04-03T04:06:25.000000Z"
            },
            "credit": 3,
            "created_at": null
        }
    ]
}
```

### HTTP Request
`GET api/courses`


<!-- END_0ec32a5c7dac7b493d908412c6b29324 -->

<!-- START_7adfcfdea10d30f89cf1c74a69c31361 -->
## Store Course

Menambah Course

> Example request:

```bash
curl -X POST \
    "http://localhost/api/courses" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"user_id":1,"name":"PABW","credit":6}'

```

```javascript
const url = new URL(
    "http://localhost/api/courses"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "user_id": 1,
    "name": "PABW",
    "credit": 6
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/courses`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `user_id` | integer |  required  | The user_id.
        `name` | string |  required  | The name.
        `credit` | integer |  required  | The credit.
    
<!-- END_7adfcfdea10d30f89cf1c74a69c31361 -->

<!-- START_d73040cd87a5f3ff21b26c94d9146513 -->
## Fetch Course

Menampilan course yang dipilih

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/courses/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/courses/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": [
        {
            "id": null,
            "user_id": 7,
            "name": "Omnis necessitatibus earum provident ut enim sunt.",
            "status": null,
            "dosen": {
                "id": 7,
                "name": "Dr. Cruz Wehner",
                "username": "harris.pauline",
                "role": "dosen",
                "credit": null,
                "created_at": "2020-04-03T04:06:25.000000Z"
            },
            "credit": 3,
            "created_at": null
        },
        {
            "id": null,
            "user_id": 3,
            "name": "Tempora similique odit perspiciatis deserunt.",
            "status": null,
            "dosen": {
                "id": 3,
                "name": "Jaleel Crooks",
                "username": "mike.feil",
                "role": "dosen",
                "credit": null,
                "created_at": "2020-04-03T04:06:24.000000Z"
            },
            "credit": 4,
            "created_at": null
        }
    ]
}
```

### HTTP Request
`GET api/courses/{course}`


<!-- END_d73040cd87a5f3ff21b26c94d9146513 -->

<!-- START_bb5a98f4f88ceacd9c6cef8cf663b402 -->
## Update Course

Mengupdate course yang dipilih

> Example request:

```bash
curl -X PUT \
    "http://localhost/api/courses/1?user_id=1&name=PABW&credit=6" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/courses/1"
);

let params = {
    "user_id": "1",
    "name": "PABW",
    "credit": "6",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/courses/{course}`

`PATCH api/courses/{course}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `user_id` |  optional  | int: The user_id.
    `name` |  optional  | string: The name.
    `credit` |  optional  | int: The credit.

<!-- END_bb5a98f4f88ceacd9c6cef8cf663b402 -->

<!-- START_ddc71fecb200b23443e0cfaad85d4241 -->
## Delete Course

Menghapus cource yang dipilih

> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/courses/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/courses/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/courses/{course}`


<!-- END_ddc71fecb200b23443e0cfaad85d4241 -->

<!-- START_3bc270ec287b2a7395a087ea729f714c -->
## User&#039;s Courses

Mendapatkan course yang telah diambil oleh user (mahasiswa).

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/user-courses/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/user-courses/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "error": "Token is not provided"
}
```

### HTTP Request
`GET api/user-courses/{user}`


<!-- END_3bc270ec287b2a7395a087ea729f714c -->

#Credit


APIs for managing credit
<!-- START_713d9f2243c83d635dfa7f73b9fc9c65 -->
## Fetch All Credit

Menampilkan semua Credit

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/credits" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/credits"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": [
        {
            "id": null,
            "user_id": 9,
            "credit": 24,
            "mahasiswa": "Dr. Jade Aufderhar III",
            "created_at": null
        },
        {
            "id": null,
            "user_id": 5,
            "credit": 24,
            "mahasiswa": "Koby Rau Sr.",
            "created_at": null
        }
    ]
}
```

### HTTP Request
`GET api/credits`


<!-- END_713d9f2243c83d635dfa7f73b9fc9c65 -->

<!-- START_173344d46219203eb6ee59b11cf651a9 -->
## Store Credit

Menambah Credit

> Example request:

```bash
curl -X POST \
    "http://localhost/api/credits" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"user_id":2,"credit":6}'

```

```javascript
const url = new URL(
    "http://localhost/api/credits"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "user_id": 2,
    "credit": 6
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/credits`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `user_id` | integer |  required  | The user_id.
        `credit` | integer |  required  | The credit.
    
<!-- END_173344d46219203eb6ee59b11cf651a9 -->

<!-- START_9e424d547e3a143386f81b2ce2e0f0b8 -->
## Fetch Credit

Menampilan Credit yang dipilih

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/credits/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/credits/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": [
        {
            "id": null,
            "user_id": 13,
            "credit": 24,
            "mahasiswa": "Aurelie Murray",
            "created_at": null
        },
        {
            "id": null,
            "user_id": 9,
            "credit": 24,
            "mahasiswa": "Dr. Jade Aufderhar III",
            "created_at": null
        }
    ]
}
```

### HTTP Request
`GET api/credits/{credit}`


<!-- END_9e424d547e3a143386f81b2ce2e0f0b8 -->

<!-- START_99bc300538d7b6352f1b85c7babe0090 -->
## Update Credit

Mengupdate Credit yang dipilih

> Example request:

```bash
curl -X PUT \
    "http://localhost/api/credits/1?user_id=1&credit=6" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/credits/1"
);

let params = {
    "user_id": "1",
    "credit": "6",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/credits/{credit}`

`PATCH api/credits/{credit}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `user_id` |  optional  | int: The user_id.
    `credit` |  optional  | int: The credit.

<!-- END_99bc300538d7b6352f1b85c7babe0090 -->

<!-- START_16a6aaa588461490fb11e6ea73514af9 -->
## Delete Credit

Menghapus cource yang dipilih

> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/credits/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/credits/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/credits/{credit}`


<!-- END_16a6aaa588461490fb11e6ea73514af9 -->

#Periode


APIs for managing periode
<!-- START_3a63e2c7307c040ab6e957848eaee2be -->
## Fetch All Periode

Menampilkan semua Periode

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/periodes" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/periodes"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": [
        {
            "id": null,
            "year": 2021,
            "semester": 7,
            "register_start": {
                "date": "2019-06-04 08:20:57.000000",
                "timezone_type": 3,
                "timezone": "UTC"
            },
            "register_end": {
                "date": "2019-10-24 03:47:57.000000",
                "timezone_type": 3,
                "timezone": "UTC"
            },
            "created_at": null
        },
        {
            "id": null,
            "year": 2020,
            "semester": 6,
            "register_start": {
                "date": "2019-05-14 12:55:08.000000",
                "timezone_type": 3,
                "timezone": "UTC"
            },
            "register_end": {
                "date": "2019-12-21 11:16:04.000000",
                "timezone_type": 3,
                "timezone": "UTC"
            },
            "created_at": null
        }
    ]
}
```

### HTTP Request
`GET api/periodes`


<!-- END_3a63e2c7307c040ab6e957848eaee2be -->

<!-- START_009283631aabf4629fa54c92f7742712 -->
## Store Periode

Menambah Periode

> Example request:

```bash
curl -X POST \
    "http://localhost/api/periodes" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"year":2020,"semester":6,"register_start":"2020-01-01","register_end":"2020-02-02"}'

```

```javascript
const url = new URL(
    "http://localhost/api/periodes"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "year": 2020,
    "semester": 6,
    "register_start": "2020-01-01",
    "register_end": "2020-02-02"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/periodes`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `year` | integer |  required  | The year.
        `semester` | integer |  required  | The semester.
        `register_start` | date |  required  | The register_start.
        `register_end` | date |  required  | The register_end.
    
<!-- END_009283631aabf4629fa54c92f7742712 -->

<!-- START_91f1a70aae781e8337715945c7d0f5fb -->
## Fetch Periode

Menampilan Periode yang dipilih

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/periodes/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/periodes/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": [
        {
            "id": null,
            "year": 2018,
            "semester": 8,
            "register_start": {
                "date": "2019-08-24 19:18:24.000000",
                "timezone_type": 3,
                "timezone": "UTC"
            },
            "register_end": {
                "date": "2019-11-15 10:24:59.000000",
                "timezone_type": 3,
                "timezone": "UTC"
            },
            "created_at": null
        },
        {
            "id": null,
            "year": 2018,
            "semester": 1,
            "register_start": {
                "date": "2019-07-02 05:32:42.000000",
                "timezone_type": 3,
                "timezone": "UTC"
            },
            "register_end": {
                "date": "2020-03-28 08:37:25.000000",
                "timezone_type": 3,
                "timezone": "UTC"
            },
            "created_at": null
        }
    ]
}
```

### HTTP Request
`GET api/periodes/{periode}`


<!-- END_91f1a70aae781e8337715945c7d0f5fb -->

<!-- START_6faf24d049919160afc698aa5341b215 -->
## Update Periode

Mengupdate Periode yang dipilih

> Example request:

```bash
curl -X PUT \
    "http://localhost/api/periodes/1?year=2020&semester=6&register_start=2020-01-01&register_end=2020-02-02" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/periodes/1"
);

let params = {
    "year": "2020",
    "semester": "6",
    "register_start": "2020-01-01",
    "register_end": "2020-02-02",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/periodes/{periode}`

`PATCH api/periodes/{periode}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `year` |  optional  | int: The year.
    `semester` |  optional  | int: The semester.
    `register_start` |  optional  | date: The register_start.
    `register_end` |  optional  | date: The register_end.

<!-- END_6faf24d049919160afc698aa5341b215 -->

<!-- START_5a54356c460c3c575553918bf6514453 -->
## Delete Periode

Menghapus cource yang dipilih

> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/periodes/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/periodes/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/periodes/{periode}`


<!-- END_5a54356c460c3c575553918bf6514453 -->

<!-- START_876b35c869b6bcb562883fb72f8337fb -->
## Periode AKtif

Mengetahui semester yang aktif

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/semester-aktif" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/semester-aktif"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "error": "Token is not provided"
}
```

### HTTP Request
`GET api/semester-aktif`


<!-- END_876b35c869b6bcb562883fb72f8337fb -->

#Study


APIs for managing study
<!-- START_501cfd20f0db4954fb5e14115daf5bf7 -->
## Fetch All Study

Menampilkan semua Study

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/studies" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/studies"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": [
        {
            "id": null,
            "user_id": 5,
            "periode_id": 2,
            "course_id": 16,
            "grade": 2,
            "mahasiswa": {
                "id": 5,
                "name": "Koby Rau Sr.",
                "username": "sunny.zulauf",
                "role": "mahasiswa",
                "credit": {
                    "id": 8,
                    "user_id": 5,
                    "credit": 24,
                    "mahasiswa": "Koby Rau Sr.",
                    "created_at": "2020-04-03T04:06:30.000000Z"
                },
                "created_at": "2020-04-03T04:06:24.000000Z"
            },
            "course": {
                "id": 16,
                "user_id": 2,
                "name": "Fuga numquam autem eos repellat aliquam.",
                "status": null,
                "dosen": {
                    "id": 2,
                    "name": "Delaney O'Kon",
                    "username": "fharvey",
                    "role": "dosen",
                    "credit": null,
                    "created_at": "2020-04-03T04:06:24.000000Z"
                },
                "credit": 3,
                "created_at": "2020-04-03T04:06:29.000000Z"
            },
            "periode": {
                "id": 2,
                "year": 2018,
                "semester": 7,
                "register_start": "2020-02-02",
                "register_end": "2020-02-17",
                "created_at": "2020-04-03T04:06:29.000000Z"
            },
            "created_at": null
        },
        {
            "id": null,
            "user_id": 14,
            "periode_id": 10,
            "course_id": 1,
            "grade": 4,
            "mahasiswa": {
                "id": 14,
                "name": "Dr. America Satterfield",
                "username": "kvandervort",
                "role": "mahasiswa",
                "credit": null,
                "created_at": "2020-04-03T04:06:28.000000Z"
            },
            "course": {
                "id": 1,
                "user_id": 8,
                "name": "Beatae et quo molestias laudantium.",
                "status": null,
                "dosen": {
                    "id": 8,
                    "name": "Mylene Williamson",
                    "username": "noel.walker",
                    "role": "dosen",
                    "credit": null,
                    "created_at": "2020-04-03T04:06:25.000000Z"
                },
                "credit": 4,
                "created_at": "2020-04-03T04:06:25.000000Z"
            },
            "periode": {
                "id": 10,
                "year": 2018,
                "semester": 6,
                "register_start": "2020-03-30",
                "register_end": "2020-04-03",
                "created_at": "2020-04-03T04:06:29.000000Z"
            },
            "created_at": null
        }
    ]
}
```

### HTTP Request
`GET api/studies`


<!-- END_501cfd20f0db4954fb5e14115daf5bf7 -->

<!-- START_bfc7049a84a170d6728cd0b251f93731 -->
## Store Study

Menambah Study

> Example request:

```bash
curl -X POST \
    "http://localhost/api/studies" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"user_id":1,"periode_id":3,"course_id":6}'

```

```javascript
const url = new URL(
    "http://localhost/api/studies"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "user_id": 1,
    "periode_id": 3,
    "course_id": 6
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/studies`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `user_id` | integer |  required  | The user_id.
        `periode_id` | integer |  required  | The periode_id.
        `course_id` | integer |  required  | The study_id.
    
<!-- END_bfc7049a84a170d6728cd0b251f93731 -->

<!-- START_a4bf0595dc99497fdc7a9914e6f39418 -->
## Fetch Study

Menampilan Study yang dipilih

> Example request:

```bash
curl -X GET \
    -G "http://localhost/api/studies/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/studies/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": [
        {
            "id": null,
            "user_id": 9,
            "periode_id": 9,
            "course_id": 4,
            "grade": 2,
            "mahasiswa": {
                "id": 9,
                "name": "Dr. Jade Aufderhar III",
                "username": "mathilde65",
                "role": "mahasiswa",
                "credit": {
                    "id": 1,
                    "user_id": 9,
                    "credit": 24,
                    "mahasiswa": "Dr. Jade Aufderhar III",
                    "created_at": "2020-04-03T04:06:30.000000Z"
                },
                "created_at": "2020-04-03T04:06:25.000000Z"
            },
            "course": {
                "id": 4,
                "user_id": 7,
                "name": "Quo explicabo ratione earum optio est autem.",
                "status": null,
                "dosen": {
                    "id": 7,
                    "name": "Dr. Cruz Wehner",
                    "username": "harris.pauline",
                    "role": "dosen",
                    "credit": null,
                    "created_at": "2020-04-03T04:06:25.000000Z"
                },
                "credit": 2,
                "created_at": "2020-04-03T04:06:25.000000Z"
            },
            "periode": {
                "id": 9,
                "year": 2018,
                "semester": 6,
                "register_start": "2019-04-16",
                "register_end": "2019-11-15",
                "created_at": "2020-04-03T04:06:29.000000Z"
            },
            "created_at": null
        },
        {
            "id": null,
            "user_id": 14,
            "periode_id": 9,
            "course_id": 20,
            "grade": 4,
            "mahasiswa": {
                "id": 14,
                "name": "Dr. America Satterfield",
                "username": "kvandervort",
                "role": "mahasiswa",
                "credit": null,
                "created_at": "2020-04-03T04:06:28.000000Z"
            },
            "course": {
                "id": 20,
                "user_id": 18,
                "name": "Atque sit quam aut cupiditate sed.",
                "status": null,
                "dosen": {
                    "id": 18,
                    "name": "Johnpaul Tromp",
                    "username": "garry57",
                    "role": "dosen",
                    "credit": null,
                    "created_at": "2020-04-03T04:06:28.000000Z"
                },
                "credit": 5,
                "created_at": "2020-04-03T04:06:29.000000Z"
            },
            "periode": {
                "id": 9,
                "year": 2018,
                "semester": 6,
                "register_start": "2019-04-16",
                "register_end": "2019-11-15",
                "created_at": "2020-04-03T04:06:29.000000Z"
            },
            "created_at": null
        }
    ]
}
```

### HTTP Request
`GET api/studies/{study}`


<!-- END_a4bf0595dc99497fdc7a9914e6f39418 -->

<!-- START_c7e77effc297f1d8d68184906e3acd37 -->
## Update Study

Mengupdate Study yang dipilih

> Example request:

```bash
curl -X PUT \
    "http://localhost/api/studies/1?user_id=1&periode_id=3&course_id=6&grade=6" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/studies/1"
);

let params = {
    "user_id": "1",
    "periode_id": "3",
    "course_id": "6",
    "grade": "6",
};
Object.keys(params)
    .forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/studies/{study}`

`PATCH api/studies/{study}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `user_id` |  optional  | int: The user_id.
    `periode_id` |  optional  | int: The periode_id.
    `course_id` |  optional  | int: The course_id.
    `grade` |  optional  | int: The semester.

<!-- END_c7e77effc297f1d8d68184906e3acd37 -->

<!-- START_e7abfaf8a60f073017d172ff8f9552d1 -->
## Delete Study

Menghapus cource yang dipilih

> Example request:

```bash
curl -X DELETE \
    "http://localhost/api/studies/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost/api/studies/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/studies/{study}`


<!-- END_e7abfaf8a60f073017d172ff8f9552d1 -->


