<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Course;
use Faker\Generator as Faker;
use App\User;

$factory->define(Course::class, function (Faker $faker) {
    return [
      'credit' => $faker->numberBetween(2, 6),
      'name' => $faker->sentence,
      'user_id' => function(){
          return User::where('role','dosen')->get()->random();
      },
    ];
});
