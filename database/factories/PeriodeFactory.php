<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Periode;
use Faker\Generator as Faker;

$factory->define(Periode::class, function (Faker $faker) {
  $startingDate = $faker->dateTimeThisYear('+1 month');
  $endingDate   = $faker->dateTimeBetween($startingDate, strtotime('+6 days'));

    return [
      'year' => $faker->numberBetween(2018, 2021),
      'semester' => $faker->numberBetween(1,8),
      'register_start' => $startingDate,
      'register_end' => $endingDate,
    ];
});
