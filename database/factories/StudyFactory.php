<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Study;
use Faker\Generator as Faker;
use App\User;
use App\Model\Course;
use App\Model\Periode;


$factory->define(Study::class, function (Faker $faker) {
    return [
      'user_id' => function(){
          return User::where('role','mahasiswa')->get()->random();
      },
      'course_id' => function(){
          return Course::all()->random();
      },
      'periode_id' => function(){
        return Periode::all()->random();
      },
      'grade' => $faker->numberBetween(1,4),
    ];
});
