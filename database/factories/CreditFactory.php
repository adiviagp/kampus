<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Credit;
use Faker\Generator as Faker;
use App\User;

$factory->define(Credit::class, function (Faker $faker) {
    return [
      'credit' => 24,
      'user_id' => function(){
          return User::where('role','mahasiswa')->get()->random();
      },
    ];
});
