<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Model\Course;
use App\Model\Periode;
use App\Model\Study;
use App\Model\Credit;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      factory(User::class,10)->create();
      factory(Course::class,10)->create();
      factory(Periode::class,10)->create();
      factory(Study::class,10)->create();
      factory(Credit::class,10)->create();

      $this->call(UserSeeder::class);
    }
}
