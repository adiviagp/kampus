<?php

use Illuminate\Database\Seeder;
use App\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $user = [
         ['name' => 'admin','username'=>'admin','role'=>'admin','password'=>bcrypt('12345678')],
         ['name' => 'dosen','username'=>'dosen','role'=>'dosen','password'=>bcrypt('12345678')],
         ['name' => 'mahasiswa','username'=>'mahasiswa','role'=>'mahasiswa','password'=>bcrypt('12345678')],
      ];

      User::insert($user);
    }
}
